#include <stddef.h>
#include <stdarg.h>
#include <setjmp.h>
extern "C" {
#include <cmocka.h>
}

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#include "iptype.h"
#include "test_iptype.h"

void test_iptype_ipv4(void **state)
{
    static const ipv4_t      ip4val = ipv4_from_digits(192,168,1,255);
    static const char* const ip4str = "192.168.1.255";

    // IP digits

    assert_int_equal(ip4val.dig[0], 192);
    assert_int_equal(ip4val.dig[1], 168);
    assert_int_equal(ip4val.dig[2], 1);
    assert_int_equal(ip4val.dig[3], 255);

    // IP integer and string conversion

    assert_int_equal(ipv4_to_int(ip4val), 0xC0A801FF);
    assert_int_equal(ipv4_from_int(0xC0A801FF).val, ip4val.val);
#ifndef NETSOCK_NO_STDCPP
    assert_string_equal(IPv4ToStr(ip4val).c_str(), ip4str);
#endif
    assert_int_equal(ipv4_from_str(ip4str).val, ip4val.val);

    // Broadcast IP calculation

    ipv4_t ip          = ipv4_from_digits(192,168,  1,  1);
    ipv4_t mask        = ipv4_from_digits(255,255,255,  0);
    ipv4_t broadcastip = ipv4_from_digits(192,168,  1,255);
    assert_int_equal(ipv4_calc_broadcast(ip, mask).val, broadcastip.val);
}

void test_iptype_ipv6(void **state)
{
    static const ipv4_t      ip4val = ipv4_from_digits(192,168,1,255);
    static const char* const ip4str = "192.168.1.255";

    static const ipv6_t      ip6val       = ipv6_from_digits(0,0,0,0,0,0xFFFF,0xC0A8,0x01FF);  // 192.168.1.255
    static const char* const ip6str_input = "::ffff:192.168.1.255";
#ifndef NETSOCK_NO_STDCPP
    static const char* const ip6str_full  = "0000:0000:0000:0000:0000:FFFF:C0A8:01FF";
    static const char* const ip6str_short = "::FFFF:C0A8:1FF";
#endif

    // IP digits

    assert_int_equal(ipv6_get_digit(ip6val, 0), 0);
    assert_int_equal(ipv6_get_digit(ip6val, 1), 0);
    assert_int_equal(ipv6_get_digit(ip6val, 2), 0);
    assert_int_equal(ipv6_get_digit(ip6val, 3), 0);
    assert_int_equal(ipv6_get_digit(ip6val, 4), 0);
    assert_int_equal(ipv6_get_digit(ip6val, 5), 0xFFFF);
    assert_int_equal(ipv6_get_digit(ip6val, 6), 0xC0A8);
    assert_int_equal(ipv6_get_digit(ip6val, 7), 0x01FF);

    // IP type and string conversion

#ifndef NETSOCK_NO_STDCPP
    assert_string_equal(IPv6ToStrFull(ip6val).c_str(), ip6str_full);
    assert_string_equal(IPv6ToStrShort(ipv6_from_digits(1,2,3,4,5,6,7,8)).c_str(), "1:2:3:4:5:6:7:8");
    assert_string_equal(IPv6ToStrShort(ipv6_from_digits(1,2,0,0,0,0,7,8)).c_str(), "1:2::7:8");
    assert_string_equal(IPv6ToStrShort(ipv6_from_digits(0,0,0,0,5,6,7,8)).c_str(), "::5:6:7:8");
    assert_string_equal(IPv6ToStrShort(ipv6_from_digits(1,2,3,4,0,0,0,0)).c_str(), "1:2:3:4::");
    assert_string_equal(IPv6ToStrShort(ip6val).c_str(), ip6str_short);
#endif
    assert_true( ipv6_is_equal(ipv6_from_str("1:2:3:4:5:6:7:8"), ipv6_from_digits(1,2,3,4,5,6,7,8)) );
    assert_true( ipv6_is_equal(ipv6_from_str("1:2::7:8"       ), ipv6_from_digits(1,2,0,0,0,0,7,8)) );
    assert_true( ipv6_is_equal(ipv6_from_str("::5:6:7:8"      ), ipv6_from_digits(0,0,0,0,5,6,7,8)) );
    assert_true( ipv6_is_equal(ipv6_from_str("1:2:3:4::"      ), ipv6_from_digits(1,2,3,4,0,0,0,0)) );
    assert_true( ipv6_is_equal(ipv6_from_str(ip6str_input), ip6val) );
    assert_true( ipv6_is_equal(ipv6_from_str(ip4str), ip6val) );

    // IP version conversion

    assert_true( ipv6_is_equal(ipv6_from_ipv4(ip4val), ip6val) );
}

void test_iptype_mac(void **state)
{
    static const macaddr_t  macaddr = macaddr_from_digits(0x11,0x22,0xCA,0xFB,0x44,0x55);
    static const char      *macstr  = "11:22:CA:FB:44:55";

    // MAC digits

    assert_int_equal(macaddr.dig[0], 0x11);
    assert_int_equal(macaddr.dig[1], 0x22);
    assert_int_equal(macaddr.dig[2], 0xCA);
    assert_int_equal(macaddr.dig[3], 0xFB);
    assert_int_equal(macaddr.dig[4], 0x44);
    assert_int_equal(macaddr.dig[5], 0x55);

    // MAC type and string conversion

#ifndef NETSOCK_NO_STDCPP
    assert_string_equal(MacAddrToStr(macaddr).c_str(), macstr);
#endif
    assert_true( macaddr_is_equal(macaddr_from_str(macstr), macaddr) );
}
