#include <string.h>
#include <threads.h>
#include <stdio.h>
#include <setjmp.h>

#ifdef _WIN32
#   include <windows.h>
#else
#   include <unistd.h>
#endif

extern "C" {
#include <cmocka.h>
}

#ifdef __BORLANDC__
#   pragma hdrstop
#endif

#include "socktcp.h"
#include "test_tcp_peer_connection.h"

#define CLIENT_REQUEST_MESSAGE  "Client request message"
#define SERVER_RESPONSE_MESSAGE "Server response message"

struct server_status
{
    bool ready;
    uint16_t port;
};

static
void WaitServerReady(struct server_status *server_status)
{
    while( !server_status->ready )
    {
#ifdef _WIN32
        Sleep(1);
#else
        usleep(1);
#endif
    }
}

static
int ServerProc(struct server_status *server_status)
{
    // Start lesson and print information

    SocketTcp server;
    assert_return_code(server.Listen(SocketAddr(Socket::AnyIP, Socket::AnyPort), false),
                       SOCKTCP_STATE_READY);
    server.SetBlockMode();

    server_status->port = server.GetLocalAddr().GetPort();
    server_status->ready = true;

    printf("[Server]: Opened.\n");
#ifndef NETSOCK_NO_STDCPP
    printf("[Server]: Local IP=%s, port=%u\n",
           IPv4ToStr(server.GetLocalAddr().GetIP()).c_str(),
           server.GetLocalAddr().GetPort());
#endif

    // Get new peer and print information

    SocketTcp *peer = server.GetNewConnect();
    assert_non_null(peer);
    peer->SetBlockMode();
    printf("[Server]: New connection.\n");

#ifndef NETSOCK_NO_STDCPP
    printf("[Server]: Peer local IP=%s, port=%u\n",
           IPv4ToStr(peer->GetLocalAddr().GetIP()).c_str(),
           peer->GetLocalAddr().GetPort());
    printf("[Server]: Peer remote IP=%s, port=%u\n",
           IPv4ToStr(peer->GetRemoteAddr().GetIP()).c_str(),
           peer->GetRemoteAddr().GetPort());
#endif

    server.Close();

    // Receive

    char msg[32] = {0};
    int size = peer->Receive(msg, sizeof(msg));
    printf("[Server]: Recv: size=%d, message=\"%s\"\n", size, msg);
    assert_int_equal(size, strlen(CLIENT_REQUEST_MESSAGE));
    assert_int_equal(0, strcmp(msg, CLIENT_REQUEST_MESSAGE));

    // Send

    size = peer->Send(SERVER_RESPONSE_MESSAGE, strlen(SERVER_RESPONSE_MESSAGE));
    printf("[Server]: Send: size=%d\n", size);
    assert_int_equal(size, strlen(SERVER_RESPONSE_MESSAGE));

    peer->Release();
    return 0;
}

static
int ClientProc(struct server_status *server_status)
{
    // Connect and print information

    WaitServerReady(server_status);

    SocketTcp client;
    assert_return_code(client.Connect(SocketAddr(Socket::LoopIP, server_status->port)),
                       SOCKTCP_STATE_READY);
    client.SetBlockMode();
    printf("[Client]: Connected\n");

#ifndef NETSOCK_NO_STDCPP
    printf("[Client]: Local IP=%s, port=%u\n",
           IPv4ToStr(client.GetLocalAddr().GetIP()).c_str(),
           client.GetLocalAddr().GetPort());
    printf("[Client]: Remote IP=%s, Port=%u\n",
           IPv4ToStr(client.GetRemoteAddr().GetIP()).c_str(),
           client.GetRemoteAddr().GetPort());
#endif

    // Empty receive test

    client.SetNonblockMode();

    char buf[32] = {0};
    int size = client.Receive(buf, sizeof(buf));
    printf("[Client]: Recv (empty test): size=%d\n", size);
    assert_int_equal(size, 0);

    client.SetBlockMode();

    // Send

    size = client.Send(CLIENT_REQUEST_MESSAGE, strlen(CLIENT_REQUEST_MESSAGE));
    printf("[Client]: Send: size=%d\n", size);
    assert_int_equal(size, strlen(CLIENT_REQUEST_MESSAGE));

    // Receive

    char msg[32] = {0};
    size = client.Receive(msg, sizeof(msg));
    printf("[Client]: Recv: size=%d, message=\"%s\"\n", size, msg);
    assert_int_equal(size, strlen(SERVER_RESPONSE_MESSAGE));
    assert_int_equal(0, strcmp(msg, SERVER_RESPONSE_MESSAGE));

    client.Close();
    return 0;
}

void test_tcp_peer_connection(void **state)
{
    struct server_status server_status =
    {
        .ready = false,
        .port = Socket::AnyPort,
    };

    thrd_t server_thread;
    assert_int_equal(thrd_success, thrd_create(&server_thread, (int(*)(void*))ServerProc, &server_status));

    thrd_t client_thread;
    assert_int_equal(thrd_success, thrd_create(&client_thread, (int(*)(void*))ClientProc, &server_status));

    int temp;
    assert_int_equal(thrd_success, thrd_join(server_thread, &temp));
    assert_int_equal(thrd_success, thrd_join(client_thread, &temp));
}
