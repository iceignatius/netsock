#ifndef _TEST_IPTYPE_H_
#define _TEST_IPTYPE_H_

void test_iptype_ipv4(void **state);
void test_iptype_ipv6(void **state);
void test_iptype_mac(void **state);

#endif
