#include <stdarg.h>
#include <setjmp.h>
#include <string.h>
extern "C" {
#include <cmocka.h>
}

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#include "urladdr.h"
#include "test_urladdr.h"

void test_urladdr_iphost(void **state)
{
    sockaddr_t addr;

    addr = urladdr_get_addr_from_url("192.168.0.127", 123);
    assert_int_equal(sockaddr_get_ip(&addr).val, ipv4_from_str("192.168.0.127").val );
    assert_int_equal(sockaddr_get_port(&addr), 123);

    addr = urladdr_get_addr_from_url("192.168.0.127:456", 123);
    assert_int_equal(sockaddr_get_ip(&addr).val, ipv4_from_str("192.168.0.127").val );
    assert_int_equal(sockaddr_get_port(&addr), 456);

    addr = urladdr_get_addr_from_url("http://192.168.0.127", 123);
    assert_int_equal(sockaddr_get_ip(&addr).val, ipv4_from_str("192.168.0.127").val );
    assert_int_equal(sockaddr_get_port(&addr), 80);
}

void test_urladdr_namehost(void **state)
{
    sockaddr_t addr;

    addr = urladdr_get_addr_from_url("https://www.w3.org/", 80);
    assert_true(sockaddr_is_available(&addr));
    assert_int_equal(sockaddr_get_port(&addr), 443);
}
