#include <string.h>
#include <threads.h>
#include <stdio.h>
#include <setjmp.h>

#ifdef _WIN32
#   include <windows.h>
#else
#   include <unistd.h>
#endif

extern "C" {
#include <cmocka.h>
}

#ifdef __BORLANDC__
#   pragma hdrstop
#endif

#include "sockudp.h"
#include "test_udp_peer_connection.h"

#define CLIENT_REQUEST_MESSAGE  "Client request message"
#define SERVER_RESPONSE_MESSAGE "Server response message"

struct server_status
{
    bool ready;
    uint16_t port;
};

static
void WaitServerReady(struct server_status *server_status)
{
    while( !server_status->ready )
    {
#ifdef _WIN32
        Sleep(1);
#else
        usleep(1);
#endif
    }
}

static
int ServerProc(struct server_status *server_status)
{
    // Open port and print information

    SocketUdp peer;
    assert_true( peer.Open(SocketAddr(Socket::AnyIP, Socket::AnyPort)) );
    peer.SetBlockMode();

    server_status->port = peer.GetLocalAddr().GetPort();
    server_status->ready = true;

    printf("[Server]: Opened.\n");
#ifndef NETSOCK_NO_STDCPP
    printf("[Server]: Local IP=%s, port=%u\n",
           IPv4ToStr(peer.GetLocalAddr().GetIP()).c_str(),
           peer.GetLocalAddr().GetPort());
    printf("[Server]: Remote IP=%s, port=%u\n",
           IPv4ToStr(peer.GetRemoteAddr().GetIP()).c_str(),
           peer.GetRemoteAddr().GetPort());
#endif

    // Receive

    SocketAddr addr;
    char msg[32] = {0};
    int size = peer.ReceiveFrom(msg, sizeof(msg), addr);
    printf("[Server]: Recv: size=%d message=\"%s\"\n", size, msg);
    assert_int_equal(size, strlen(CLIENT_REQUEST_MESSAGE));
    assert_int_equal(0, strcmp(msg, CLIENT_REQUEST_MESSAGE));

#ifndef NETSOCK_NO_STDCPP
    printf("[Server]: Recv: IP=%s, port=%u\n",
           IPv4ToStr(addr.GetIP()).c_str(),
           addr.GetPort());
#endif

    // Send

    size = peer.SendTo(SERVER_RESPONSE_MESSAGE, strlen(SERVER_RESPONSE_MESSAGE), addr);
    printf("[Server]: Send: size=%d\n", size);
    assert_int_equal(size, strlen(SERVER_RESPONSE_MESSAGE));

    peer.Close();
    return 0;
}

static
int ClientProc(struct server_status *server_status)
{
    // Open port and print information

    WaitServerReady(server_status);

    SocketUdp peer;
    assert_true( peer.Open(SocketAddr(Socket::AnyIP, Socket::AnyPort)) );
    peer.SetBlockMode();
    printf("[Client]: Opened.\n");

    assert_true( peer.EnableBroadcast() );
    peer.SetRemoteAddr(SocketAddr(Socket::BroadcastIP, server_status->port));

#ifndef NETSOCK_NO_STDCPP
    printf("[Client]: Local IP=%s, port=%u\n",
           IPv4ToStr(peer.GetLocalAddr().GetIP()).c_str(),
           peer.GetLocalAddr().GetPort());
    printf("[Client]: Remote IP=%s, port=%u\n",
           IPv4ToStr(peer.GetRemoteAddr().GetIP()).c_str(),
           peer.GetRemoteAddr().GetPort());
    printf("[Client]: Broadcast IP=%s\n",
           IPv4ToStr(peer.GetBroadcastIP()).c_str());
#endif

    // Empty receive test

    peer.SetNonblockMode();

    SocketAddr addr;
    char buf[32] = {0};
    int size = peer.ReceiveFrom(buf, sizeof(buf), addr);
    printf("[Client]: Recv (Empty Test): size=%d\n", size);
    assert_int_equal(size, 0);

#ifndef NETSOCK_NO_STDCPP
    printf("[Client]: Recv: IP=%s, port=%u\n",
           IPv4ToStr(addr.GetIP()).c_str(),
           addr.GetPort());
#endif

    peer.SetBlockMode();

    // Send

    size = peer.Send(CLIENT_REQUEST_MESSAGE, strlen(CLIENT_REQUEST_MESSAGE));
    printf("[Client]: Send: size=%d\n", size);
    assert_int_equal(size, strlen(CLIENT_REQUEST_MESSAGE));

    // Receive

    char msg[32] = {0};
    size = peer.ReceiveFrom(msg, sizeof(msg), addr);
    printf("[Client]: Recv: size=%d message=\"%s\"\n", size, msg);
    assert_int_equal(size, strlen(SERVER_RESPONSE_MESSAGE));
    assert_int_equal(0, strcmp(msg, SERVER_RESPONSE_MESSAGE));

#ifndef NETSOCK_NO_STDCPP
    printf("[Client]: Recv: IP=%s, port=%u\n",
           IPv4ToStr(addr.GetIP()).c_str(),
           addr.GetPort());
#endif

    peer.Close();
    return 0;
}

void test_udp_peer_connection(void **state)
{
    struct server_status server_status =
    {
        .ready = false,
        .port = Socket::AnyPort,
    };

    thrd_t server_thread;
    assert_int_equal(thrd_success, thrd_create(&server_thread, (int(*)(void*))ServerProc, &server_status));

    thrd_t client_thread;
    assert_int_equal(thrd_success, thrd_create(&client_thread, (int(*)(void*))ClientProc, &server_status));

    int temp;
    assert_int_equal(thrd_success, thrd_join(server_thread, &temp));
    assert_int_equal(thrd_success, thrd_join(client_thread, &temp));
}
