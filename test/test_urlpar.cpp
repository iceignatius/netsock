#include <stdarg.h>
#include <setjmp.h>
#include <string.h>
extern "C" {
#include <cmocka.h>
}

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#include "urlpar.h"
#include "test_urlpar.h"

void test_urlpar_full(void **state)
{
    static const char url[] = "scheme://user:password@domain:port/path?query_string#fragment_id";
    char buf[256];

    memset(buf, -1, sizeof(buf));
    assert_non_null( urlpar_extract_sceme(buf, sizeof(buf), url) );
    assert_string_equal(buf, "scheme");

    memset(buf, -1, sizeof(buf));
    assert_non_null( urlpar_extract_user(buf, sizeof(buf), url) );
    assert_string_equal(buf, "user");

    memset(buf, -1, sizeof(buf));
    assert_non_null( urlpar_extract_pass(buf, sizeof(buf), url) );
    assert_string_equal(buf, "password");

    memset(buf, -1, sizeof(buf));
    assert_non_null( urlpar_extract_host(buf, sizeof(buf), url) );
    assert_string_equal(buf, "domain");

    memset(buf, -1, sizeof(buf));
    assert_non_null( urlpar_extract_port(buf, sizeof(buf), url) );
    assert_string_equal(buf, "port");

    memset(buf, -1, sizeof(buf));
    assert_non_null( urlpar_extract_path(buf, sizeof(buf), url) );
    assert_string_equal(buf, "path");

    memset(buf, -1, sizeof(buf));
    assert_non_null( urlpar_extract_query(buf, sizeof(buf), url) );
    assert_string_equal(buf, "query_string");

    memset(buf, -1, sizeof(buf));
    assert_non_null( urlpar_extract_fragid(buf, sizeof(buf), url) );
    assert_string_equal(buf, "fragment_id");
}

void test_urlpar_query(void **state)
{
    static const char url[] = "scheme://domain:port/path?query_string";
    char buf[256];

    memset(buf, -1, sizeof(buf));
    assert_non_null( urlpar_extract_sceme(buf, sizeof(buf), url) );
    assert_string_equal(buf, "scheme");

    memset(buf, -1, sizeof(buf));
    assert_null( urlpar_extract_user(buf, sizeof(buf), url) );

    memset(buf, -1, sizeof(buf));
    assert_null( urlpar_extract_pass(buf, sizeof(buf), url) );

    memset(buf, -1, sizeof(buf));
    assert_non_null( urlpar_extract_host(buf, sizeof(buf), url) );
    assert_string_equal(buf, "domain");

    memset(buf, -1, sizeof(buf));
    assert_non_null( urlpar_extract_port(buf, sizeof(buf), url) );
    assert_string_equal(buf, "port");

    memset(buf, -1, sizeof(buf));
    assert_non_null( urlpar_extract_path(buf, sizeof(buf), url) );
    assert_string_equal(buf, "path");

    memset(buf, -1, sizeof(buf));
    assert_non_null( urlpar_extract_query(buf, sizeof(buf), url) );
    assert_string_equal(buf, "query_string");

    memset(buf, -1, sizeof(buf));
    assert_null( urlpar_extract_fragid(buf, sizeof(buf), url) );
}

void test_urlpar_path(void **state)
{
    static const char url[] = "scheme://domain/path";
    char buf[256];

    memset(buf, -1, sizeof(buf));
    assert_non_null( urlpar_extract_sceme(buf, sizeof(buf), url) );
    assert_string_equal(buf, "scheme");

    memset(buf, -1, sizeof(buf));
    assert_null( urlpar_extract_user(buf, sizeof(buf), url) );

    memset(buf, -1, sizeof(buf));
    assert_null( urlpar_extract_pass(buf, sizeof(buf), url) );

    memset(buf, -1, sizeof(buf));
    assert_non_null( urlpar_extract_host(buf, sizeof(buf), url) );
    assert_string_equal(buf, "domain");

    memset(buf, -1, sizeof(buf));
    assert_null( urlpar_extract_port(buf, sizeof(buf), url) );

    memset(buf, -1, sizeof(buf));
    assert_non_null( urlpar_extract_path(buf, sizeof(buf), url) );
    assert_string_equal(buf, "path");

    memset(buf, -1, sizeof(buf));
    assert_null( urlpar_extract_query(buf, sizeof(buf), url) );

    memset(buf, -1, sizeof(buf));
    assert_null( urlpar_extract_fragid(buf, sizeof(buf), url) );
}

void test_urlpar_domain_port(void **state)
{
    static const char url[] = "domain:port";
    char buf[256];

    memset(buf, -1, sizeof(buf));
    assert_null( urlpar_extract_sceme(buf, sizeof(buf), url) );

    memset(buf, -1, sizeof(buf));
    assert_null( urlpar_extract_user(buf, sizeof(buf), url) );

    memset(buf, -1, sizeof(buf));
    assert_null( urlpar_extract_pass(buf, sizeof(buf), url) );

    memset(buf, -1, sizeof(buf));
    assert_non_null( urlpar_extract_host(buf, sizeof(buf), url) );
    assert_string_equal(buf, "domain");

    memset(buf, -1, sizeof(buf));
    assert_non_null( urlpar_extract_port(buf, sizeof(buf), url) );
    assert_string_equal(buf, "port");

    memset(buf, -1, sizeof(buf));
    assert_null( urlpar_extract_path(buf, sizeof(buf), url) );

    memset(buf, -1, sizeof(buf));
    assert_null( urlpar_extract_query(buf, sizeof(buf), url) );

    memset(buf, -1, sizeof(buf));
    assert_null( urlpar_extract_fragid(buf, sizeof(buf), url) );
}

void test_urlpar_domain(void **state)
{
    static const char url[] = "domain";
    char buf[256];

    memset(buf, -1, sizeof(buf));
    assert_null( urlpar_extract_sceme(buf, sizeof(buf), url) );

    memset(buf, -1, sizeof(buf));
    assert_null( urlpar_extract_user(buf, sizeof(buf), url) );

    memset(buf, -1, sizeof(buf));
    assert_null( urlpar_extract_pass(buf, sizeof(buf), url) );

    memset(buf, -1, sizeof(buf));
    assert_non_null( urlpar_extract_host(buf, sizeof(buf), url) );
    assert_string_equal(buf, "domain");

    memset(buf, -1, sizeof(buf));
    assert_null( urlpar_extract_port(buf, sizeof(buf), url) );

    memset(buf, -1, sizeof(buf));
    assert_null( urlpar_extract_path(buf, sizeof(buf), url) );

    memset(buf, -1, sizeof(buf));
    assert_null( urlpar_extract_query(buf, sizeof(buf), url) );

    memset(buf, -1, sizeof(buf));
    assert_null( urlpar_extract_fragid(buf, sizeof(buf), url) );
}

void test_urlpar_ports(void **state)
{
    assert_int_equal(urlpar_get_port("http://domain_name:123"), 123);
    assert_int_equal(urlpar_get_port("http://domain_name:456"), 456);
    assert_int_equal(urlpar_get_port("http://domain_name"), 80);
    assert_int_equal(urlpar_get_port("https://domain_name"), 443);
    assert_int_equal(urlpar_get_port("smtps://domain_name"), 465);
    assert_int_equal(urlpar_get_port("domain_name"), 0);
}
