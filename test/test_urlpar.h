#ifndef _TEST_URLPAR_H_
#define _TEST_URLPAR_H_

void test_urlpar_full(void **state);
void test_urlpar_query(void **state);
void test_urlpar_path(void **state);
void test_urlpar_domain_port(void **state);
void test_urlpar_domain(void **state);
void test_urlpar_ports(void **state);

#endif
