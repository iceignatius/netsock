#include <stddef.h>
#include <stdarg.h>
#include <setjmp.h>
extern "C" {
#include <cmocka.h>
}

#include "test_urlpar.h"
#include "test_iptype.h"
#include "test_request_host_ip.h"
#include "test_tcp_peer_connection.h"
#include "test_udp_peer_connection.h"
#include "test_urladdr.h"

int main(int argc, char *argv[])
{
    struct CMUnitTest tests[] =
    {
        cmocka_unit_test(test_urlpar_full),
        cmocka_unit_test(test_urlpar_query),
        cmocka_unit_test(test_urlpar_path),
        cmocka_unit_test(test_urlpar_domain_port),
        cmocka_unit_test(test_urlpar_domain),
        cmocka_unit_test(test_urlpar_ports),
        cmocka_unit_test(test_iptype_ipv4),
        cmocka_unit_test(test_iptype_ipv6),
        cmocka_unit_test(test_iptype_mac),
        cmocka_unit_test(test_request_host_ip),
        cmocka_unit_test(test_tcp_peer_connection),
        cmocka_unit_test(test_udp_peer_connection),
        cmocka_unit_test(test_urladdr_iphost),
        cmocka_unit_test(test_urladdr_namehost),
    };

    return cmocka_run_group_tests_name("netsock_test", tests, NULL, NULL);
}
