#include <stddef.h>
#include <stdarg.h>
#include <setjmp.h>
#include <stdio.h>
extern "C" {
#include <cmocka.h>
}

#include "sockbase.h"
#include "test_request_host_ip.h"

void test_request_host_ip(void **state)
{
    ipv4_t ip = socknet_get_ip_by_hostname("www.google.com");
    assert_int_not_equal(ip.val, 0);
#ifndef NETSOCK_NO_STDCPP
    printf("[Get IP]: IP of \"www.google.com\": %s\n", IPv4ToStr(ip).c_str());
#endif

    ip = socknet_get_ip_by_hostname("tw.yahoo.com");
    assert_int_not_equal(ip.val, 0);
#ifndef NETSOCK_NO_STDCPP
    printf("[Get IP]: IP of \"tw.yahoo.com\": %s\n", IPv4ToStr(ip).c_str());
#endif
}
