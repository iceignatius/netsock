#include <assert.h>
#include <stdlib.h>

#ifdef __linux__
#include <unistd.h>
#include <errno.h>
#include <sys/socket.h>
#include <sys/select.h>
#endif

#ifdef _WIN32
#include <winsock2.h>
#endif

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#include "winwsa.h"
#include "socktcp.h"

void socktcp_init(socktcp_t *self)
{
    /**
     * @memberof socktcp_t
     * @brief Constructor.
     *
     * @param self Object instance.
     */
    assert( self );

    winwsa_init_recursive();
    self->socket = sockfd_get_invalid();
}

void socktcp_destroy(socktcp_t *self)
{
    /**
     * @memberof socktcp_t
     * @brief Destructor.
     *
     * @param self Object instance.
     */
    assert( self );

    socktcp_close(self);
    winwsa_deinit_recursive();
}

socktcp_t* socktcp_create(sockfd_t socket)
{
    /**
     * @memberof socktcp_t
     * @brief Dynamic create a socktcp_t object.
     *
     * @param socket The system socket file descriptor to be used by this object,
     *               and that socket file descriptor will be close when object be released.
     * @return Return the socket object create if succeed; and NULL if failed.
     */
    socktcp_t *self    = NULL;
    bool       succeed = false;

    do
    {
        self = malloc(sizeof(socktcp_t));
        if( !self ) break;

        socktcp_init(self);
        self->socket = socket;

        succeed = true;
    } while(false);

    if( !succeed )
    {
        if( self )
        {
            free(self);
            self = NULL;
        }
    }

    return self;
}

void socktcp_release(socktcp_t *self)
{
    /**
     * @memberof socktcp_t
     * @brief Release a dynamic created socktcp_t object.
     *
     * @param self Object instance.
     *
     * @warning Call this function on a dynamic created object only!
     */
    assert( self );

    socktcp_destroy(self);
    free(self);
}

sockfd_t socktcp_get_fd(socktcp_t *self)
{
    /**
     * @memberof socktcp_t
     * @brief Get the OS depend socket file descriptor.
     *
     * @param self Object instance.
     * @return The socket file descriptor.
     */
    assert( self );

    return self->socket;
}

void socktcp_set_block_flag(socktcp_t *self, bool block_mode)
{
    /**
     * @memberof socktcp_t
     * @brief Set blocking mode by flag.
     *
     * @param self       Object instance.
     * @param block_mode TRUE to set blocking mode; and FALSE to set non-blocking mode.
     */
    assert( self );

    sockfd_set_block_flag(self->socket, block_mode);
}

void socktcp_set_block_mode(socktcp_t *self)
{
    /**
     * @memberof socktcp_t
     * @brief Set to blocking mode.
     *
     * @param self Object instance.
     */
    assert( self );

    sockfd_set_block_flag(self->socket, true);
}

void socktcp_set_nonblock_mode(socktcp_t *self)
{
    /**
     * @memberof socktcp_t
     * @brief Set to non-blocking mode.
     *
     * @param self Object instance.
     */
    assert( self );

    sockfd_set_block_flag(self->socket, false);
}

sockaddr_t socktcp_get_local_addr(const socktcp_t *self)
{
    /**
     * @memberof socktcp_t
     * @brief Get address information of the local host.
     *
     * @param self Object instance.
     * @return Address information of the local host.
     *
     * @remarks This function will be succeed only if the socket have a connection.
     */
    assert( self );

    return sockfd_get_local_addr(self->socket);
}

sockaddr_t socktcp_get_remote_addr(const socktcp_t *self)
{
    /**
     * @memberof socktcp_t
     * @brief Get address information of the remote host.
     *
     * @param self Object instance.
     * @return Address information of the remote host.
     *
     * @remarks This function will be succeed only if the object have a connection.
     */
    assert( self );

    return sockfd_get_remote_addr(self->socket);
}

static
int start_connect(sockfd_t fd, const sockaddr_t *addr)
{
    if( !connect(fd, (const struct sockaddr*)addr, sizeof(sockaddr_t)) )
        return SOCKTCP_STATE_READY;

#if   defined(__linux__)
    if( EINPROGRESS == errno ) return SOCKTCP_STATE_BUSY;
#elif defined(_WIN32)
    if( WSAEWOULDBLOCK == winwsa_get_last_error_code() ) return SOCKTCP_STATE_BUSY;
#else
    #error No implementation on this platform!
#endif

    return SOCKTCP_STATE_ERROR;
}

static
int wait_get_state(sockfd_t fd, unsigned timeout)
{
    if( !sockfd_is_valid(fd) ) return SOCKTCP_STATE_CLOSED;

    fd_set wlist;
    FD_ZERO(&wlist);
    FD_SET(fd, &wlist);

    fd_set elist;
    FD_ZERO(&elist);
    FD_SET(fd, &elist);

    struct timeval tv =
    {
        .tv_sec  = timeout ? timeout / 1000 : 0,
        .tv_usec = timeout ? timeout % 1000 * 1000 : 0,
    };

    if( 0 >= select(fd+1, NULL, &wlist, &elist, &tv) )
    {
#ifdef __unix__
        if( errno == EINTR || errno == EAGAIN )
            return SOCKTCP_STATE_BUSY;
#endif
        return SOCKTCP_STATE_ERROR;
    }

    if( FD_ISSET(fd, &elist) ) return SOCKTCP_STATE_ERROR;
    if( !FD_ISSET(fd, &wlist) ) return SOCKTCP_STATE_BUSY;

#if   defined(__linux__)
    int       errval;
    socklen_t valsize = sizeof(errval);
    if( getsockopt(fd, SOL_SOCKET, SO_ERROR, &errval, &valsize) ) return SOCKTCP_STATE_ERROR;
    if( errval ) return SOCKTCP_STATE_ERROR;
#elif defined(_WIN32)
    int errval;
    int valsize = sizeof(errval);
    if( getsockopt(fd, SOL_SOCKET, SO_ERROR, (char*)&errval, &valsize) ) return SOCKTCP_STATE_ERROR;
    if( errval ) return SOCKTCP_STATE_ERROR;
#else
    #error No implementation on this platform!
#endif

    return SOCKTCP_STATE_READY;
}

int socktcp_connect(socktcp_t *self, const sockaddr_t *addr, unsigned timeout)
{
    /**
     * @memberof socktcp_t
     * @brief Connect to a specified remote host.
     *
     * @param self    Object instance.
     * @param addr    The remote address.
     * @param timeout Time out value in milliseconds.
     *                The system default value will be used if
     *                the input value is large than that.
     *                This parameter can be ZERO to make function return immediately and not wait,
     *                please see remarks for more details.
     * @retval ::SOCKTCP_STATE_READY Connection established and successfully.
     * @retval ::SOCKTCP_STATE_ERROR Connection failed.
     * @retval ::SOCKTCP_STATE_BUSY  The connection procedure has started but not done yet.
     *
     * @remarks
     *     @li If parameter @a timeout value is not zero, then
     *         the return value will be ::SOCKTCP_STATE_READY or ::SOCKTCP_STATE_BUSY only!
     *     @li If parameter @a timeout value is zero, then
     *         this function will return immediately, and
     *         the connect work may not be done and is still in progressing.
     *         In this case, the return value will be ::SOCKTCP_STATE_BUSY, and
     *         user will need to call socktcp_t::socktcp_get_state continuously
     *         to wait for it be ready, or be failed, or abort waiting.
     *     @li The new connection will be in non-blocking mode,
     *         and need be change to blocking mode if you want to using it under blocking mode.
     *     @li This function will terminate the listen process if the object is listening.
     */
    assert( self );

    socktcp_close(self);

    int res = SOCKTCP_STATE_ERROR;
    do
    {
        if( !winwsa_start_explicit() )
            break;

        self->socket = socket(AF_INET, SOCK_STREAM, 0);
        if( !sockfd_is_valid(self->socket) )
            break;

        sockfd_set_block_flag(self->socket, false);

        res = start_connect(self->socket, addr);
        if( timeout && res == SOCKTCP_STATE_BUSY )
            res = wait_get_state(self->socket, timeout);

    } while( false );

    if( res == SOCKTCP_STATE_ERROR ) socktcp_close(self);

    return res;
}

int socktcp_listen(socktcp_t *self, const sockaddr_t *localaddr, bool reuse)
{
    /**
     * @memberof socktcp_t
     * @brief Starting to listen the client connect request.
     *
     * @param self      Object instance.
     * @param localaddr Set the local listening address :
     *                  @li The IP can be ZERO (::ipv4_const_any) to let system select a valid IP to use.
     *                      But note that we cannot get the IP that system selected later.
     *                  @li The port can be set to a valid number manually,
     *                      or just set to ZERO to let system select a valid port number.
     *                      And we can get the system selected port number later.
     * @param reuse     Set TRUE to enable port reuse behaviour; and set FALSE to disable.
     * @retval ::SOCKTCP_STATE_READY The socket is ready to use.
     * @retval ::SOCKTCP_STATE_ERROR Failed to start listen.
     *
     * @remarks
     *     @li The new connection will be in non-blocking mode,
     *         and need be change to blocking mode if you want to using it under blocking mode.
     *     @li This function will terminate the current connection if the object is connecting to a host.
     *     @li You can use socktcp_t::socktcp_get_new_connect to check and get the new connections
     *         after the object go in listening process succeed.
     */
    assert( self );

    socktcp_close(self);

    int res = SOCKTCP_STATE_ERROR;
    do
    {
        if( !winwsa_start_explicit() ) break;

        self->socket = socket(AF_INET, SOCK_STREAM, 0);
        if( !sockfd_is_valid(self->socket) ) break;

#ifdef __linux__
        if( reuse )
        {
            static const int flag = 1;
            if( setsockopt(self->socket, SOL_SOCKET, SO_REUSEADDR, (char*)&flag, sizeof(flag)) )
                break;
        }
#endif

        if( bind(self->socket, (const struct sockaddr*)localaddr, sizeof(sockaddr_t)) ) break;
        if( listen(self->socket, SOMAXCONN) ) break;

        sockfd_set_block_flag(self->socket, false);

        res = SOCKTCP_STATE_READY;
    } while( false );

    if( res == SOCKTCP_STATE_ERROR ) socktcp_close(self);

    return res;
}

void socktcp_close(socktcp_t *self)
{
    /**
     * @memberof socktcp_t
     * @brief Terminate the current connection or listen process.
     *
     * @param self Object instance.
     */
    assert( self );

    if( sockfd_is_valid(self->socket) )
    {
#if   defined(__linux__)
        close(self->socket);
#elif defined(_WIN32)
        closesocket(self->socket);
#else
    #error No implementation on this platform!
#endif
        self->socket = sockfd_get_invalid();
    }
}

int socktcp_get_state(const socktcp_t *self)
{
    /**
     * @memberof socktcp_t
     * @brief Get current state of the socket.
     *
     * @param self The object to be operated.
     * @return One of the values defined in ::socktcp_state.
     */
    return wait_get_state(self->socket, 0);
}

socktcp_t* socktcp_get_new_connect(socktcp_t *self)
{
    /**
     * @memberof socktcp_t
     * @brief Check and get the new client connection.
     *
     * @param self Object instance.
     * @return A TCP socket object will be returned if there have a new connection be established;
     *         and NULL will be returned otherwise.
     *
     * @remarks
     *     @li This function will net get any connection if the object is not in listening process,
     *         you will need to call socktcp_listen to start listen process.
     *     @li You can make your communication with the client by using the object returned by this function,
     *         but remember that, you MUST call socktcp_release to release the object when the time
     *         you not need the connection any more.
     *     @li The new connection will be in non-blocking mode,
     *         and need be change to blocking mode if you want to using it under blocking mode.
     */
    sockfd_t    peerfd;
    sockaddr_t  peeraddr;
    socktcp_t  *peer;
#ifdef __linux__
    socklen_t   addrsz = sizeof(sockaddr_t);
#else
    int         addrsz = sizeof(sockaddr_t);
#endif

    assert( self );

    // Check new connect
    peerfd = accept(self->socket, (struct sockaddr*)&peeraddr, &addrsz);
    if( !sockfd_is_valid(peerfd) ) return NULL;

    // Create peer object
    peer = socktcp_create(peerfd);
    if( !peer ) return NULL;

    // Set peer to non-block mode
    socktcp_set_nonblock_mode(peer);

    return peer;
}

int socktcp_send(socktcp_t *self, const void *data, size_t size)
{
    /**
     * @memberof socktcp_t
     * @brief Send data to the remote host.
     *
     * @param self Object instance.
     * @param data Data to send.
     * @param size Size of data to send.
     * @return The size sent if succeed; and -1 if failed.
     */
    assert( self );

    return sockfd_send(self->socket, data, size);
}

int socktcp_receive(socktcp_t *self, void *buf, size_t size)
{
    /**
     * @memberof socktcp_t
     * @brief Receive data from the remote host.
     *
     * @param self Object instance.
     * @param buf  A buffer to receive data.
     * @param size Size of the buffer.
     * @return The size received if succeed; and -1 if failed.
     */
    assert( self );

    return sockfd_receive(self->socket, buf, size);
}
