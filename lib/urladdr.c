#include "urlpar.h"
#include "urladdr.h"

sockaddr_t urladdr_get_addr_from_url(const char *url, uint16_t defaultport)
{
    /**
     * Query host address by URL
     *
     * @param url           The input URL string.
     * @param defaultport   The default port which will be used
     *                      if the port cannot be parsed from the URL.
     * @return A valid address if success; or an invalid address otherwise.
     */
    sockaddr_t addr;
    sockaddr_init(&addr);

    do
    {
        char hostname[64] = {0};
        if( !urlpar_extract_host(hostname, sizeof(hostname)-1, url) )
            break;

        ipv4_t ip = ipv4_from_str(hostname);
        if( !ip.val ) ip = socknet_get_ip_by_hostname(hostname);
        if( !ip.val ) break;

        uint16_t port = urlpar_get_port(url);
        if( !port ) port = defaultport;

        sockaddr_init_value(&addr, ip, port);

    } while(false);

    return addr;
}
