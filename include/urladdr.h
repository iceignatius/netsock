/**
 * @file
 * @brief     URL address getter.
 * @details   Query host address by URL.
 * @author    王文佑
 * @date      2020/09/25
 * @copyright ZLib Licence
 * @see       https://gitlab.com/iceignatius/netsock
 */
#ifndef _NETSOCK_URLADDR_H_
#define _NETSOCK_URLADDR_H_

#include "sockbase.h"

#ifdef __cplusplus
extern "C" {
#endif

sockaddr_t urladdr_get_addr_from_url(const char *url, uint16_t defaultport);

#ifdef __cplusplus
}   // extern "C"
#endif

#ifdef __cplusplus

/// C++ wrapper of URL address getter.
namespace UrlAddr
{

#ifndef NETSOCK_NO_STDCPP
inline SocketAddr GetAddrFromUrl(const std::string &url, uint16_t defaultport)
{
    /// @see ::urladdr_get_addr_from_url
    return urladdr_get_addr_from_url(url.c_str(), defaultport);
}
#endif

}   // namespace UrlAddr

#endif  // __cplusplus

#endif
